package combiner

import (
	"os"
	"sync"
	"path/filepath"

	"math"
	"log"
	"time"
)

var m sync.Mutex

func moveFiles(paths []string, newPath string, done chan interface{}) {

	for _, path := range paths {
		files, err := os.Open(path)

		if err != nil {
			log.Fatal(err)
		}
		defer files.Close()

		allFiles, err := files.Readdir(0)

		if err != nil {
			log.Fatal(err)
		}

		for _, file := range allFiles {
			location := filepath.Join(newPath, file.Name())
			oldLocation := filepath.Join(path, file.Name())

			m.Lock()
			os.Rename(oldLocation, location)
			m.Unlock()
		}
	}

	time.Sleep(time.Second * 1)
	done <- struct{}{}
}

func Combiner(paths []string, newPath string) {

	ncor_ := math.Log2(float64(len(paths)))
	ncor := int(ncor_)

	if ncor == 0 {
		ncor = 1
	}

	done := make(chan interface{}, ncor)
	split := len(paths) / ncor
	j := 0
	for i := 0; i < ncor; i++ {
		go moveFiles(paths[j:split], newPath, done)

		j = split
		split += split

		if split > len(paths) {
			split = len(paths)
		}
	}

	for {
		time.Sleep(time.Second * 1)
		if len(done) == ncor {
			close(done)
			break
		}
	}
}
