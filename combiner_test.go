package combiner

import (
	"testing"

	"os"
	"path/filepath"
	"log"

	"strconv"
)

var files []string
var paths []string

func makeDummyFiles() {
	for i := 0; i < 50; i++ {
		path := "D:\\dummy" + strconv.Itoa(i)
		paths = append(paths, path)
	}

	files = []string{
		"a", "b", "c",
		"d", "e", "f",
		"g", "h", "i",
		"j", "k", "l",
		"m", "n", "o",
		"p", "q", "r",
		"s", "t", "u",
		"v", "w", "x",
		"y", "z", 
	}

	for _, path := range paths {
		os.Mkdir(path, os.FileMode(int(0777)))
	}

	for _, file := range files {
		for i, path := range paths {
			filePath := filepath.Join(path, file + strconv.Itoa(i) + ".txt")
		
			f, err := os.Create(filePath)
			if err != nil {
				log.Fatal(err)
			}
			defer f.Close()
		}
	}
}

func cleanFiles(path string) {
	err := os.RemoveAll(path)

	if err != nil {
		log.Fatal(err)
	}
}

func TestCombiner(t * testing.T) {
	makeDummyFiles()

	path := "D:\\dummyComb"
	os.Mkdir(path, os.FileMode(0777))
	Combiner(paths, path)

	count := 0

	current, _ := os.Open(path)
	files_, err := current.Readdir(0)

	if err != nil {
		log.Fatal(err)
	}

	for _, _ = range files_ {
		count++
	}

	if count != len(files) * len(paths) {
		t.Errorf("Expected %d files, but found %d", len(files), count)
	}

	cleanFiles(path)
	for _, path := range paths {
		cleanFiles(path)
	}
}