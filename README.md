# Combiner

1. Get

`go get bitbucket.org/mon3ita/combiner`

2. Example

```
package yourpackage

import 'bitbucket.org/mon3ita/splitter'

func yourFunction() {
    combiner.Combiner(filePaths, newDestination)
}
```